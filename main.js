require('dotenv').config();
const express = require('express');
const app = express();
const morgan = require('morgan');

const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(morgan('dev'));

app.get('/', (req, res) => {
    res.status(200).json({
        status: true,
        message: 'welcome to contoh-docker-image'
    });
});

app.listen(PORT, () => {
    console.log('running on port', PORT);
});